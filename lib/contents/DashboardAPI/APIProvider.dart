import 'package:http/http.dart';
import 'dart:convert' as convert;
import 'package:skripsi_wins/Utils/utils.dart';

class DashboardAPIProvider {
  Client client = Client();
  var util = new Util();

  saveUserDatatoBackend(String name, username, password, role, email) async {
    String url = util.getConfiguration()['base_url']+'dashboard/regis/';
    Map<String, String> data = {
      "name": name,
      "username": username,
      "password": password,
      "role": role,
      "email": email
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '400';
    }
  }

  dynamic checkLoginData(String username, String password) async {
    String url = util.getConfiguration()['base_url']+'dashboard/login/';
    Map<String, String> data = {
      "username": username,
      "password": password,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return "500";
    }
  }

  checkProject(String username) async {
    String url = util.getConfiguration()['base_url']+'dashboard/checksm/';
    Map<String, String> data = {
      "username": username
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      if (jsonResponse['result'] == 'no_project') {
        return 'no_project';
      } else {
        return jsonResponse['proj_id'];
      }
    } else {
      return 500;
    }
  }

  createProject(String username, projName, dod, date, int sprintDays) async {
    String url = util.getConfiguration()['base_url']+'dashboard/createproject/';
    Map<String, dynamic> data = {
      "username": username,
      "proj_name": projName,
      "dod": dod,
      "date": date,
      "sprint_days": sprintDays
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getTasks(int ProjID) async {
    String url = util.getConfiguration()['base_url']+'dashboard/getdatascrumboard/';
    Map<String, int> data = {
      "proj_id": ProjID,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  detailTask(int task_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/detailtask/';
    Map<String, int> data = {
      "task_id": task_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  updateTask(Map<String, dynamic> mapUpdate) async {
    String url = util.getConfiguration()['base_url']+'dashboard/edittask/';
    var body = convert.json.encode(mapUpdate);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  detailProfile(String username) async {
    String url = util.getConfiguration()['base_url']+'dashboard/detailprofil/';
    Map<String, String> data = {
      "username": username,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  updateProfile(Map<String, String> mapUpdate) async {
    String url = util.getConfiguration()['base_url']+'dashboard/update/';
    var body = convert.json.encode(mapUpdate);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getPeople(int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/getpeople/';
    Map<String, int> data = {
      "proj_id": proj_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  addPerson(int proj_id, String username, role, date) async {
    String url = util.getConfiguration()['base_url']+'dashboard/addperson/';
    Map<String, dynamic> data = {
      "proj_id": proj_id,
      "username": username,
      "role": role,
      "date": date
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  findPerson(String username) async {
    String url = util.getConfiguration()['base_url']+'dashboard/findperson/';
    Map<String, String> data = {
      "username": username,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getDev(int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/getdev/';
    Map<String, int> data = {
      "proj_id": proj_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  addTask(int proj_id, priority, velocity, status_task, String task_owner, desc, task_name, sprint_name, date) async {
    String url = util.getConfiguration()['base_url']+'dashboard/addtask/';
    Map<String, dynamic> data = {
      "proj_id": proj_id,
      "task_owner": task_owner,
      "desc": desc,
      "priority": priority,
      "task_name": task_name,
      "velocity": velocity,
      "status_task": status_task,
      "sprint_name": sprint_name,
      "date": date
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  updateDod(int proj_id, String dod) async {
    String url = util.getConfiguration()['base_url']+'dashboard/editdod/';
    Map<String, dynamic> data = {
      "proj_id": proj_id,
      "dod": dod,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  burndownData(String username, date1, flag, int proj_id, Map<dynamic, dynamic> burndown) async {
    String url = util.getConfiguration()['base_url']+'dashboard/insertburndown/';
    Map<String, dynamic> data = {
      "username": username,
      "proj_id": proj_id,
      "data": burndown,
      "date1" : date1,
      "flag": flag
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getPOData(int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/dashboardpo/';
    Map<String, int> data = {
      "proj_id": proj_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getDevData(String username, dateNow, day, int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/dashboarddev/';
    Map<String, dynamic> data = {
      "username": username,
      "date_now": dateNow,
      "proj_id": proj_id,
      "day": day
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getSMData(int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/dashboardsm/';
    Map<String, int> data = {
      "proj_id": proj_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  checkUser(String username) async {
    String url = util.getConfiguration()['base_url']+'dashboard/checkuser/';
    Map<String, String> data = {
      "username": username,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  checkPOSB(int proj_id, String date) async {
    String url = util.getConfiguration()['base_url']+'dashboard/checkposb/';
    Map<String, dynamic> data = {
      "proj_id": proj_id,
      "date": date
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getHistory(int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/gethistory/';
    Map<String, dynamic> data = {
      "proj_id": proj_id,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  checkposbc(String username, date, day, int proj_id) async {
    String url = util.getConfiguration()['base_url']+'dashboard/checkposbc/';
    Map<String, dynamic> data = {
      "username": username,
      "proj_id": proj_id,
      "date": date,
      "day": day
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }

  getListProjects(String username) async {
    String url = util.getConfiguration()['base_url']+'dashboard/listproject/';
    Map<String, dynamic> data = {
      "username": username,
    };
    var body = convert.json.encode(data);
    Map<String, String> userHeader = {"Content-type": "application/json", "Accept": "application/json"};
    // Await the http get response, then decode the json-formatted response.
    var response = await client.post(url,body:body,headers:userHeader);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    } else {
      return '500';
    }
  }
}