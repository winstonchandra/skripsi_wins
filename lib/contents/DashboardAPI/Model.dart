class Task {
  int task_id;
  String task_name;
  String pic;
  String status_task;
  String priority;
  String sprint_name;
  Task(
      {this.task_id, this.task_name, this.pic, this.status_task, this.priority, this.sprint_name});
}

class Person {
  String username;
  String fullname;
  String role;

  Person(
      {this.username, this.fullname, this.role});
}

class SprintBurndownExpect {
  DateTime date;
  int velocities;

  SprintBurndownExpect(this.date, this.velocities);
}

class SprintBurndownRem {
  DateTime date;
  int velocities;

  SprintBurndownRem(this.date, this.velocities);
}

class DevDonePerDayVel {
  DateTime date;
  int velocities;

  DevDonePerDayVel(this.date, this.velocities);
}

class DevRemPerDayVel {
  DateTime date;
  int velocities;

  DevRemPerDayVel(this.date, this.velocities);
}

class SMData {
  String name;
  int velocities;

  SMData(this.name, this.velocities);
}

class HistoryData {
  String sprint_name;
  int velocities;

  HistoryData(this.sprint_name, this.velocities);
}

class Project {
  String proj_name;
  int proj_id;

  Project(this.proj_name, this.proj_id);
}