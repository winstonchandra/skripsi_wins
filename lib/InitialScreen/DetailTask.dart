import 'package:flutter/material.dart';
import 'package:skripsi_wins/InitialScreen/ScrumBoard.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class DetailTask extends StatefulWidget {

  int task_id, proj_id;
  String role, sprint_name;

  DetailTask({
    this.proj_id, this.task_id, this.role, this.sprint_name
  });

  @override
  _DetailTaskState createState() => _DetailTaskState();
}

class _DetailTaskState extends State<DetailTask> {
  final formdevkey = GlobalKey<FormState>();
  final formPOkey = GlobalKey<FormState>();
  bool autoValidate = false;
  bool autoValidateDev = false;
  String task_name, desc, obstacles, priorStr, statusStr, task_owner, statusStrBegin, priorStrBegin, sprint_name;
  int velocity, task_priority, task_status, task_id;
  List<String> arrPrior = <String>["Low", "Medium", "High"].toList();
  List<String> arrStatus = <String>["In Progress","Done"].toList();
  bool isload;
  bool isVisible = false;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  Map<String, int> statusToInt = {"In Progress" : 1, "Done" : 2};
  Map<int, String> statusToString = {1 : "In Progress", 2 : "Done"};
  Map<int, String> priorToString = {0 : "Low", 1 : "Medium", 2 : "High"};
  Map<String, int> priorToInt = {"Low" : 0, "Medium" : 1, "High" : 2};
  Map<String, dynamic> mapUpdate = new Map();
  final util = new Util();
  dynamic response;
  ProgressDialog pr;

  @override
  void initState() {
    detailTask().whenComplete(() {
      setState(() {
        isload = false;
      });
    });
  }

  Future detailTask() async {
    isload = true;
    response = await apiProvider.detailTask(widget.task_id);
    if (response == '500') {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    task_id = response['data']['task_id'];
    task_name = response['data']['task_name'];
    task_owner = response['data']['task_owner'];
    desc = response['data']['desc'];
    obstacles = response['data']['obstacles'];
    velocity = response['data']['velocity'];
    task_status = response['data']['status_task'];
    statusStr = statusToString[task_status];
    statusStrBegin = statusToString[task_status];
    task_priority = response['data']['priority'];
    sprint_name = response['data']['sprint_name'];
    priorStr = priorToString[task_priority];
    priorStrBegin = priorToString[task_priority];
  }

  updateTask() async {
    mapUpdate['task_id'] = task_id;
    if (task_status == 2) {
      mapUpdate['proj_id'] = widget.proj_id;
      mapUpdate['sprint_name'] = widget.sprint_name;
      mapUpdate['velocity'] = velocity;
      mapUpdate['task_owner'] = task_owner;
      var dt = DateTime.now();
      var newFormat = DateFormat("yyyy-MM-dd");
      String date = newFormat.format(dt);
      mapUpdate['date'] = date;
    }
    response = await apiProvider.updateTask(mapUpdate);
    if (response == '500') {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      pr.hide();
      showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Perubahan Berhasil'),
              content: Text('Data task telah berhasil diubah'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
      setState(() {
        isVisible = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    Widget makeBodyPO() {
      final statusDropdownItems = arrStatus.map((String value) { return DropdownMenuItem(
        child: Text(value),
        value: value,
      );
      }).toList();

      final priorDropdownItems = arrPrior.map((String value) { return DropdownMenuItem(
        child: Text(value),
        value: value,
      );
      }).toList();

      return new Container(
        // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
        child: new Form(
          key: formPOkey,
          autovalidate: autoValidate,
          child: new Container(
            color: Color(0xffFFFFFF),
            child: Padding(
              padding: EdgeInsets.only(bottom: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Task Name",
                                  hasFloatingPlaceholder: true),
                              enabled: false,
                              initialValue: task_name,
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Task Description",
                                  hasFloatingPlaceholder: true),
                              validator: (String value) {
                                return value.isEmpty
                                    ? 'Please fill the data'
                                    : value.length > 2500? 'Desc length must less than 2500 characters' : null;
                              },
                              maxLines: null,
                              initialValue: desc,
                              enabled: task_status == 2? false : true,
                              onChanged: (text) {
                                desc = text;
                                mapUpdate['description'] = desc;
                                setState(() {
                                  isVisible = true;
                                });
                              },
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Task Owner",
                                  hasFloatingPlaceholder: true),
                              enabled: false,
                              initialValue: task_owner,
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Obstacles",
                                  hasFloatingPlaceholder: true),
                              initialValue: obstacles,
                              maxLines: null,
                              enabled: task_status == 2? false : true,
                              onChanged: (text) {
                                obstacles = text;
                                setState(() {
                                  mapUpdate['obstacles'] = obstacles;
                                  isVisible = true;
                                });
                              },
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Velocity",
                                  hasFloatingPlaceholder: true),
                              initialValue: velocity.toString(),
                              enabled: false,
                            ),
                          ),
                        ],
                      )),
                  task_status == 2? Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Priority",
                                  hasFloatingPlaceholder: true),
                              initialValue: priorToString[task_priority],
                              enabled: false,
                            ),
                          ),
                        ],
                      )) : Padding(
                    padding: EdgeInsets.only(left:25.0, right: 25.0),
                    child: new Row (
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Listener(
                          onPointerDown: (_) => FocusScope.of(context).unfocus(),
                          child: DropdownButton<String>(
                            icon: Icon(Icons.arrow_drop_down),
                            hint: Text("Select Priority"),
                            value: priorStr,
                            items: priorDropdownItems,
                            onChanged: (String value) {
                              setState(() {
                                if (value != priorStrBegin) {
                                  isVisible = true;
                                  priorStr = value;
                                  task_priority = priorToInt[priorStr];
                                  mapUpdate['priority'] = task_priority;
                                } else {
                                  if (mapUpdate.containsKey("priority")) {
                                    mapUpdate.remove("priority");
                                  }
                                  priorStr = value;
                                  isVisible = false;
                                }
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  task_status == 2? Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Status",
                                  hasFloatingPlaceholder: true),
                              initialValue: statusToString[task_status],
                              enabled: false,
                            ),
                          ),
                        ],
                      )) : Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0),
                    child: new Row (
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Listener(
                          onPointerDown: (_) => FocusScope.of(context).unfocus(),
                          child: DropdownButton<String>(
                            icon: Icon(Icons.arrow_drop_down),
                            hint: Text("Select Status"),
                            value: statusStr,
                            items: statusDropdownItems,
                            onChanged: (String value) {
                              setState(() {
                                if (value != statusStrBegin) {
                                  isVisible = true;
                                  statusStr = value;
                                  task_status = statusToInt[statusStr];
                                  mapUpdate['status_task'] = task_status;
                                } else {
                                  setState(() {
                                    if (mapUpdate.containsKey("status_task")) {
                                      mapUpdate.remove("status_task");
                                    }
                                    statusStr = value;
                                    isVisible = false;
                                  });
                                }
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: isVisible,
                    child: Padding(
                      padding: EdgeInsets.only(left: 225.0, top: 10.0),
                      child: Container(color: Colors.redAccent, child: FlatButton(
                        textColor: Colors.white,
                        child: Text('Save'),
                        onPressed: () {
                          pr.show().whenComplete(() {
                            updateTask();
                          });
                        },
                        shape: CircleBorder(),
                      ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final makeBodyDevorSM = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Form(
        key: formdevkey,
        autovalidate: autoValidate,
        child: new Container(
          color: Color(0xffFFFFFF),
          child: Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Task Name",
                                hasFloatingPlaceholder: true),
                            enabled: false,
                            initialValue: task_name,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Task Owner",
                                hasFloatingPlaceholder: true),
                            initialValue: task_owner,
                            enabled: false,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            maxLines: null,
                            decoration: InputDecoration(
                                labelText: "Description",
                                hasFloatingPlaceholder: true),
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            enabled: false,
                            initialValue: desc,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Obstacles",
                                hasFloatingPlaceholder: true),
                            initialValue: obstacles,
                            maxLines: null,
                            enabled: task_status == 2? false : true,
                            onChanged: (text) {
                              obstacles = text;
                              setState(() {
                                mapUpdate['obstacles'] = obstacles;
                                isVisible = true;
                              });
                            },
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Velocity",
                                hasFloatingPlaceholder: true),
                            initialValue: velocity.toString(),
                            enabled: false,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Priority",
                                hasFloatingPlaceholder: true),
                            initialValue: priorToString[task_priority],
                            enabled: false,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Status Task",
                                hasFloatingPlaceholder: true),
                            initialValue: statusToString[task_status],
                            enabled: false,
                          ),
                        ),
                      ],
                    )),
                Visibility(
                  visible: isVisible,
                  child: Padding(
                    padding: EdgeInsets.only(left: 225.0, top: 15.0),
                    child: Container (color: Colors.redAccent, child:FlatButton(
                      textColor: Colors.white,
                      child: Text('Save'),
                      onPressed: () {
                        pr.show().whenComplete(() {
                          updateTask();
                        });
                      },
                      shape: CircleBorder(),
                    ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    void back() {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ScrumBoard()
        ),
      );
    }

    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: new InkWell(
            child: new Icon(
              Icons.arrow_back,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () {
              back();
            },
          ),
          centerTitle: true,
          title: Text(
            "Detail Task".toUpperCase(),
            style: TextStyle(
              color: util.hexToColor("#FFFFFF"),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: new Container(
          color: Colors.white,
          child: isload? Center(child: new CircularProgressIndicator()) : widget.role == "Product Owner"?
          new SingleChildScrollView(
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),
            child: makeBodyPO(),
          ) : makeBodyDevorSM,
        ));
  }
}