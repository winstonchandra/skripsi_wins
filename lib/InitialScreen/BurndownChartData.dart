import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/InitialScreen/Dashboard.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:intl/intl.dart';

class Burndown extends StatefulWidget {

  int proj_id;
  String username, flagFill;

  Burndown({
    this.proj_id, this.username, this.flagFill
  });

  @override
  _BurndownState createState() => _BurndownState();
}

class _BurndownState extends State<Burndown> {
  final formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  final util = new Util();
  bool isLoad;
  dynamic response;
  String day1, day2, day3, day4, day5, date1, date2, date3, date4, date5;
  int velDay1, velDay2, velDay3, velDay4, velDay5;
  Map mapData = new Map();
  List arrDays = new List();
  List arrDates = new List();
  ProgressDialog pr;

  @override
  void initState() {
    getData();
    setState(() {
      isLoad = false;
    });
  }

  getData() {
    isLoad = true;
    var currDate = DateTime.now();
    var format = DateFormat("yyyy-MM-dd");
    date1 = format.format(currDate);
    day1 = DateFormat('EEEE').format(currDate);
    if (day1 != "Saturday" && day1 != "Sunday") {
      arrDays.add(day1);
      arrDates.add(date1);
    }
    int counter = 1;
    while (arrDays.length < 5) {
      var tempDate = currDate.add(Duration(days: counter));
      String checkerDay = DateFormat('EEEE').format(tempDate);
      if ((checkerDay != "Saturday") && (checkerDay != "Sunday")) {
        arrDays.add(checkerDay);
        arrDates.add(format.format(tempDate));
      }
      counter = counter + 1;
    }
    day2 = arrDays[1]; date2 = arrDates[1];
    day3 = arrDays[2]; date3 = arrDates[2];
    day4 = arrDays[3]; date4 = arrDates[3];
    day5 = arrDays[4]; date5 = arrDates[4];
  }

  insertData() async {
    if (formkey.currentState.validate()) {
      formkey.currentState.save();
      response = await apiProvider.burndownData(widget.username, date1, widget.flagFill, widget.proj_id, mapData);
      if (response.toString() == "500") {
        pr.hide();
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Terjadi kesalahan pada server'),
                content: Text('Mohon ulangi permintaan kembali'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else {
        if (pr.isShowing()) {
          pr.hide().whenComplete(() {
            Navigator.of(context).pop();
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Dashboard()
              ),
            );
          });
        }
      }
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            autoValidate = true;
          });
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Form(
        key: formkey,
        autovalidate: autoValidate,
        child: new Container(
          color: Color(0xffFFFFFF),
          child: Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 120.0,top: 70.0, bottom: 15.0),
                    child: new Text("Burndown Chart Expectation Velocities", style: new TextStyle(fontSize: 18.0, fontFamily: 'sans-serif'),)
                ),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: day1+", "+date1,
                                hintText: "Example: 40",
                                hasFloatingPlaceholder: true),
                            keyboardType: TextInputType.number,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            onSaved: (String value) {velDay1 = int.parse(value);mapData[date1] = velDay1;},
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: day2+", "+date2,
                                hintText: "Example: 30",
                                hasFloatingPlaceholder: true),
                            keyboardType: TextInputType.number,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            onSaved: (String value) {velDay2 = int.parse(value);mapData[date2] = velDay2;},
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: day3+", "+date3,
                                hintText: "Example: 20",
                                hasFloatingPlaceholder: true),
                            keyboardType: TextInputType.number,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            onSaved: (String value) {
                              velDay3 = int.parse(value);
                              mapData[date3] = velDay3;
                              },
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: day4+", "+date4,
                                hintText: "Example: 20",
                                hasFloatingPlaceholder: true),
                            keyboardType: TextInputType.number,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            onSaved: (String value) {velDay4 = int.parse(value);mapData[date4] = velDay4;},
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: day5+", "+date5,
                                hintText: "Example: 5",
                                hasFloatingPlaceholder: true),
                            keyboardType: TextInputType.number,
                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            onSaved: (String value) {velDay5 = int.parse(value);mapData[date5] = velDay5;},
                          ),
                        ),
                      ],
                    )),
                Visibility(
                  child: Padding(
                    padding: EdgeInsets.only(left: _width/1.9, top:30.0),
                    child: new InkWell(
                      onTap: () {
                        pr.show().whenComplete(() {
                          insertData();
                        });
                      },
                      child: new Container(
                        width: util.fitScreenSize(_width, 0.4),
                        height: util.fitScreenSize(_height, 0.08),
                        decoration: new BoxDecoration(
                          color: util.hexToColor("#FF0000"),
                          border: new Border.all(
                            color: util.hexToColor("#FFFFFF"),
                            width: 2.0,
                          ),
                          borderRadius: new BorderRadius.circular(20.0),
                        ),
                        child: new Center(
                          child: new Text(
                            'Save',
                            style: new TextStyle(
                              fontSize: 15.0,
                              color: util.hexToColor("#FFFFFF"),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    return new WillPopScope(
        onWillPop: () async => false,
        child: new  Scaffold(
          resizeToAvoidBottomPadding: false,
          body: isLoad? Center(child: new CircularProgressIndicator()) : makeBody
      )
    );
  }
}