import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/InitialScreen/ScrumBoard.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:intl/intl.dart';

class AddTask extends StatefulWidget {

  int proj_id;
  String sprint_name;

  AddTask(
      {this.proj_id, this.sprint_name});

  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  final formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  final util = new Util();
  List<String> arrDev = new List();
  List<String> arrPrior = <String>["Low", "Medium", "High"].toList();
  Map<String, int> mapBackPriority = {"Low":0, "Medium": 1, "High": 2};
  dynamic response;
  bool isload;
  String task_name, desc, ownerChoosen;
  String priorityStr = "Low";
  int priority = 0;
  int velocity, status_task;
  ProgressDialog pr;

  @override
  void initState() {
    getDev().whenComplete(() {
      setState(() {
        isload = false;
      });
    });
  }

  Future getDev() async {
    isload = true;
    response = await apiProvider.getDev(widget.proj_id);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    if (response['data'].length > 0) {
      for(var i = 0; i < response['data'].length; i++) {
        arrDev.add(response['data'][i]);
      }
    } else {
      arrDev = [];
    }
  }

  addTask() async {
    status_task = 1;
    var dt = DateTime.now();
    var newFormat = DateFormat("yyyy-MM-dd");
    String date = newFormat.format(dt);
    response = await apiProvider.addTask(widget.proj_id, priority, velocity, status_task, ownerChoosen, desc, task_name, widget.sprint_name, date);
    if (response.toString() == "500") {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    pr.hide();
    Navigator.pop(context);
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ScrumBoard(),
      ),
    );
  }

  void _validateInputs() {
    if (formkey.currentState.validate() && (ownerChoosen != null)) {
      formkey.currentState.save();
      addTask();
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            autoValidate = true;
          });
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    void back() {
      Navigator.pop(context);
    }

    final devDropdownItems = arrDev.map((String value) { return DropdownMenuItem(
      child: Text(value),
      value: value,
    );
    }).toList();

    final priorDropdownItems = arrPrior.map((String value) { return DropdownMenuItem(
      child: Text(value),
      value: value,
    );
    }).toList();

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Form(
          key: formkey,
          autovalidate: autoValidate,
          child: new Container(
            color: Color(0xffFFFFFF),
            child: Padding(
              padding: EdgeInsets.only(bottom: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Sprint Name",
                                  hasFloatingPlaceholder: true),
                              initialValue: widget.sprint_name,
                              enabled: false,
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Task Name",
                                  hasFloatingPlaceholder: true),
                              validator: (String value) {
                                return value.isEmpty? 'Please fill the data' : null;
                              },
                              onSaved: (String value) {task_name = value;},
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              maxLines: null,
                              decoration: InputDecoration(
                                  labelText: "Task Description",
                                  hasFloatingPlaceholder: true),
                              validator: (String value) {
                                return value.isEmpty? 'Please fill the data' : null;
                              },
                              onSaved: (String value) {desc = value;},
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 2.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Flexible(
                            child: new TextFormField(
                              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                              decoration: InputDecoration(
                                  labelText: "Task Velocities",
                                  hasFloatingPlaceholder: true),
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please fill the data';
                                } try {
                                  int.parse(value);
                                } catch (e) {
                                  return 'Input must be integer';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              onSaved: (String value) {velocity = int.parse(value);},
                            ),
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 25.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: new Text(
                                'Task Owner',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            flex: 2,
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left:25.0,right:102.0),
                    child: Listener(
                      onPointerDown: (_) => FocusScope.of(context).unfocus(),
                      child: DropdownButton<String>(
                        icon: Icon(Icons.arrow_drop_down),
                        hint: Text("Select Developer"),
                        value: ownerChoosen,
                        items: devDropdownItems,
                        onChanged: (String value) {
                          setState(() {
                            ownerChoosen = value;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left:25.0,right: 180.0),
                    child: Text(
                      ownerChoosen == null? 'Choose Developer!': '',
                      style: TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 25.0),
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: new Text(
                                'Task Priority',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            flex: 2,
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left:25.0, right:102.0),
                    child: Listener(
                      onPointerDown: (_) => FocusScope.of(context).unfocus(),
                      child: DropdownButton<String>(
                        icon: Icon(Icons.arrow_drop_down),
                        hint: Text("Select priority"),
                        value: priorityStr,
                        items: priorDropdownItems,
                        onChanged: (String value) {
                          setState(() {
                            priorityStr = value;
                            priority = mapBackPriority[priorityStr];
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 225.0, top: 15.0),
                      child: Container (color: Colors.redAccent, child:FlatButton(
                        textColor: Colors.white,
                        child: Text('Add Task'),
                        onPressed: () {
                          pr.show().whenComplete(() {
                            _validateInputs();
                          });
                        },
                        shape: CircleBorder(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
      ),
    );

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: new InkWell(
          child: new Icon(
            Icons.arrow_back,
            color: util.hexToColor("#FFFFFF"),
          ),
          onTap: () {
            back();
          },
        ),
        centerTitle: true,
        title: Text(
          "Add Task".toUpperCase(),
          style: TextStyle(
            color: util.hexToColor("#FFFFFF"),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: isload? Center(child: new CircularProgressIndicator()) : makeBody,
    );
  }
}