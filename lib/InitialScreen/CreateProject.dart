import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skripsi_wins/InitialScreen/BurndownChartData.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:intl/intl.dart';

class CreateProject extends StatefulWidget {

  @override
  _CreateProjectState createState() => _CreateProjectState();
}

class _CreateProjectState extends State<CreateProject> {
  final formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  final util = new Util();
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  dynamic response;
  String username, projName, dod;
  int sprintDays, proj_id;
  ProgressDialog pr;

  @override
  void initState() {
    getDataCache().whenComplete(() {
      build(context);
    });
  }

  Future getDataCache() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString("username");
  }

  Future saveToCache(dynamic response) async {
    proj_id = response['data'];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("proj_id", response['data']);
  }

  void _validateInputsCreateProject() {
    if (formkey.currentState.validate()) {
      formkey.currentState.save();
      createProject().whenComplete(() {
        saveToCache(response).whenComplete(() {
          pr.hide();
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Burndown(proj_id: proj_id, username: username, flagFill: 'pertama')
            ),
          );
        });
      });
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            autoValidate = true;
          });
        });
      }
    }
  }

  Future createProject() async {
    var dt = DateTime.now();
    var newFormat = DateFormat("yyyy-MM-dd");
    String date = newFormat.format(dt);
    response = await apiProvider.createProject(username, projName, dod, date, sprintDays);
    if (response.toString() == '500') {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    return WillPopScope(
        onWillPop: () async => false,
        child: new Scaffold(
            body: new Container(
              color: Colors.white,
              child: new Form(
                key: formkey,
                autovalidate: autoValidate,
                child: new ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        new Container(
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(right: 130.0,top: 60.0, bottom: 15.0),
                                  child: new Text("Create Your Project", style: new TextStyle(fontSize: 18.0, fontFamily: 'sans-serif'),)
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          color: Color(0xffFFFFFF),
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                            child: new Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(
                                        left: 25.0, right: 32.0, top: 2.0),
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Flexible(
                                          child: new TextFormField(
                                            decoration: InputDecoration(
                                                labelText:"Project Name"
                                            ),
                                            validator: (String value) {
                                              return value.isEmpty? 'Please fill the data' : null;
                                            },
                                            onSaved: (String value) {projName = value;},
                                          ),
                                        ),
                                      ],
                                    )),
                                Padding(
                                    padding: EdgeInsets.only(
                                        left: 25.0, right: 32.0, top: 2.0),
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Flexible(
                                          child: new TextFormField(
                                            maxLines: 3,
                                            decoration: InputDecoration(
                                                labelText: "Definition of Done"
                                            ),
                                            validator: (String value) {
                                              if (value.isEmpty) {
                                                return "Please fill the data";
                                              } else if (value.length > 500) {
                                                return 'Value cannot be more than 500 characters';
                                              } else {
                                                return null;
                                              }
                                            },
                                            onSaved: (String value) {dod = value;},
                                          ),
                                        ),
                                      ],
                                    )),
                                Padding(
                                    padding: EdgeInsets.only(
                                        left: 25.0, right: 25.0, top: 2.0),
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Flexible(
                                          child: new TextFormField(
                                            keyboardType: TextInputType.number,
                                            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                                            decoration: InputDecoration(
                                                labelText: "How many days in each sprint?",
                                                hintText: "Example: 14",
                                                hasFloatingPlaceholder: true),
                                            validator: (String value) {
                                              if (value.isEmpty) {
                                                return 'Please fill the data';
                                              } try {
                                                int.parse(value);
                                              } catch (e) {
                                                return 'Input must be integer';
                                              }
                                              return null;
                                            },
                                            onSaved: (String value) {sprintDays = int.parse(value);},
                                          ),
                                        ),
                                      ],
                                    )),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 30.0),
                                  child: new InkWell(
                                    onTap: () {
                                      pr.show().whenComplete(() {
                                        _validateInputsCreateProject();
                                      });
                                    },
                                    child: new Container(
                                      width: util.fitScreenSize(_width, 0.4),
                                      height: util.fitScreenSize(_height, 0.08),
                                      decoration: new BoxDecoration(
                                        color: util.hexToColor("#FF0000"),
                                        border: new Border.all(
                                          color: util.hexToColor("#FFFFFF"),
                                          width: 2.0,
                                        ),
                                        borderRadius: new BorderRadius.circular(20.0),
                                      ),
                                      child: new Center(
                                        child: new Text(
                                          'Create',
                                          style: new TextStyle(
                                            fontSize: 15.0,
                                            color: util.hexToColor("#FFFFFF"),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ))
    );
  }
}