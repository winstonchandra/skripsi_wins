import 'package:flutter/material.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skripsi_wins/InitialScreen/HomeScreen.dart';
import 'package:skripsi_wins/InitialScreen/Profile.dart';
import 'package:skripsi_wins/InitialScreen/DetailTask.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:skripsi_wins/InitialScreen/People.dart';
import 'package:skripsi_wins/InitialScreen/AddTask.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/contents/DashboardAPI/Model.dart';

class ScrumBoard extends StatefulWidget {

  @override
  _ScrumBoardState createState() => _ScrumBoardState();
}

class _ScrumBoardState extends State<ScrumBoard> {

  var formKey, addtaskformkey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final util = new Util();
  bool isLoad;
  List<Task> arrTask = new List<Task>();
  List<String> arrSprint = new List();
  dynamic response;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  String username, role, sprint_name;
  String projName, dod = "";
  int ProjID;
  bool autoValidate, autoValidateAddTask = false;
  ProgressDialog pr;
  var arrSearchTask = new List();
  int cnt = 0;
  bool search = false;

  @override
  void initState() {
    getDataCache().whenComplete(() {
      if (role == "Product Owner") {
        checkPO().whenComplete(() {
          getTasks().whenComplete( () {
            setState(() {
              isLoad = false;
            });
          });
        });
      } else {
        getTasks().whenComplete(() {
            setState(() {
              isLoad = false;
            });
          }
        );
      }
    });
  }

  Future getDev() async {
    response = await apiProvider.getDev(ProjID);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future checkPO() async {
    var dt = DateTime.now();
    var newFormat = DateFormat("yyyy-MM-dd");
    String date = newFormat.format(dt);
    response = await apiProvider.checkPOSB(ProjID, date);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future getDataCache() async {
    isLoad = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.getString("username");
    role = prefs.getString("role");
    ProjID = prefs.getInt("proj_id");
  }

   Future getTasks() async {
    response = await apiProvider.getTasks(ProjID);
    if (response.toString() == '500') {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    else if (response['data'].length >= 1) {
      projName = response['proj_name'];
      dod = response['dod'];
      for (var i = 0; i < response['sprint_data'].length; i++) {
        arrSprint.add(response['sprint_data'][i].toString());
      }
      sprint_name = arrSprint[0];
      getArrTasks(sprint_name);
      return ;
    } else if (response['data'].length == 0) {
      arrTask = [];
      projName = response['proj_name'];
      dod = response['dod'];
      for (var i = 0; i < response['sprint_data'].length; i++) {
        arrSprint.add(response['sprint_data'][i].toString());
      }
      sprint_name = response['sprint_data'][0];
      return ;
    }
  }

  getArrTasks(String sprintName) {
    arrTask = new List<Task>();
    sprintName = sprintName.split(",")[0];
    if (response['data'].length == 0 || response['data'][sprintName] == null) {
      arrTask = [];
    } else {
      for(var i = 0; i < response['data'][sprintName].length; i++) {
        arrTask.add(new Task(task_id: response['data'][sprintName][i]['task_id'],
            task_name: response['data'][sprintName][i]['task_name'],
            pic: response['data'][sprintName][i]['task_owner'],
            status_task: response['data'][sprintName][i]['status_task'],
            priority: response['data'][sprintName][i]['priority'],
            sprint_name: response['data'][sprintName][i]['sprint_name']));
      }
    }
  }

  Widget mappingPriority(BuildContext context, String priority) {
    if (priority == 'Low') {
      return Text(priority,
          style: TextStyle(backgroundColor: Colors.green,
              color: Colors.white));
    } else if (priority == 'Medium') {
        return Text(priority,
            style: TextStyle(backgroundColor: Colors.orange,
                color: Colors.white));
    } else {
      return Text(priority,
          style: TextStyle(backgroundColor: Colors.red,
              color: Colors.white));
    }
  }

  Widget mappingStatusTask(BuildContext context, String status_task) {
    if (status_task == 'Unassigned') {
      return Text(status_task,
          style: TextStyle(backgroundColor: Colors.red,
              color: Colors.white));
    } else if (status_task == 'In Progress') {
      return Text(status_task,
          style: TextStyle(backgroundColor: Colors.grey,
              color: Colors.white));
    } else {
      return Text(status_task,
          style: TextStyle(backgroundColor: Colors.green,
              color: Colors.white));
    }
  }

  void refreshData() {
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ScrumBoard()
      ),
    );
  }

  void redirectTolistPeople() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => People(proj_id: ProjID, role: role)
      ),
    );
  }

  void filterSearch(String task_name) {
    if(task_name != "") {
      List<Task> listSearch = new List<Task>();
      arrTask.forEach((item) => item.task_name.contains(task_name)? listSearch.add(item) : print(item)
      );
      setState(() {
        arrTask.clear();
        arrTask.addAll(listSearch);
      });
      return ;
    } else {
      setState(() {
        getArrTasks(sprint_name);
      });
    }
  }

  updateDod() async {
    response = await apiProvider.updateDod(ProjID, dod);
    if (response.toString() == '500') {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Perubahan berhasil'),
              content: Text('Definition of done telah berhasil diubah'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    refreshData();
                  },
                )
              ],
            );
          });
    }
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    final sprintDropdownItems = arrSprint.map((String value) { return DropdownMenuItem(
      child: Text(value),
      value: value,
    );
    }).toList();

    ListTile makeListTile(Task task) =>
        ListTile(
          contentPadding:
          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          title: Text(
            task.task_name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

          subtitle: Row(
            children: <Widget>[
              Expanded(
                  flex: 7,
                  child: Container(
                    // tag: 'hero',
                    child: Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: mappingStatusTask(context, task.status_task)),
                  )),
              Expanded(
                flex: 5,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: mappingPriority(context, task.priority)),
              ),
              Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text("PIC: "+task.pic,
                        style: TextStyle(color: Colors.white))
                ),
              ),
            ],
          ),
          trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailTask(proj_id: ProjID, task_id: task.task_id, sprint_name: task.sprint_name, role: role)));
          },
        );

    Card makeCard(Task task) {
      return new Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child:
             new Container(
               decoration: BoxDecoration(color: Colors.blue),
               child: makeListTile(task),
             ),
      );
    }

    final makeBody = new Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Column(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 22.0, right: 22.0, top: 10.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Project Name',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(left: 22.0, right: 22.0, top: 2.0, bottom: 15.0),
              child: new Row(
                children: <Widget>[
                  new Flexible(
                    child: new TextFormField(
                      initialValue: projName,
                      enabled: false,
                    ),
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 22.0, right: 22.0, top: 10.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Sprint Name',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Listener(
              onPointerDown: (_) => FocusScope.of(context).unfocus(),
              child: DropdownButton<String>(
                icon: Icon(Icons.arrow_drop_down),
                hint: Text("Select sprint"),
                value: sprint_name,
                items: sprintDropdownItems,
                onChanged: (String value) {
                  setState(() {
                    sprint_name = value;
                    getArrTasks(sprint_name);
                  });
                },
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 22.0, right: 22.0, top: 10.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Definition of Done',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(left: 22.0, right: 22.0, top: 2.0, bottom: 15.0),
              child: new Row(
                children: <Widget>[
                  new Flexible(
                    child: new TextFormField(
                      initialValue: dod,
                      enabled: false,
                      maxLines: null,
                    ),
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 22.0, right: 22.0, top: 10.0, bottom: 5.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Tasks:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          arrTask.length == 0 && search == false?
          Padding(padding: EdgeInsets.only(top: 29.0),
              child: Center(child: Text("Periode sprint belum memiliki task"))) :
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              onChanged: (value) {
                search = true;
                filterSearch(value);
              },
              decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search Task",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
                ListView.builder(
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: arrTask.length,
                  itemBuilder: (BuildContext context, int index) {
                    return makeCard(arrTask[index]);
                  },
                ),
        ],
      ),
    );

    _openAddPickerModal(BuildContext context) {
      final flatButtonColor = Theme
          .of(context)
          .primaryColor;
      showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 170.0,
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  FlatButton(
                    textColor: flatButtonColor,
                    child: Text('Add People'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => People(proj_id: ProjID, role: role)));
                    },
                  ),
                  FlatButton(
                    textColor: flatButtonColor,
                    child: Text('Add Task'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddTask(proj_id: ProjID, sprint_name: arrSprint[0].split(',')[0],)));
                    },
                  ),
                  FlatButton(
                    textColor: flatButtonColor,
                    child: Text('Edit Definition of Done'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      return showDialog(context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Update Definition of Done'),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: new TextFormField(
                                      maxLines: 7,
                                      validator: (String value) {
                                        return value.length > 500? 'Input cannot be more than 500 characters' : null;
                                      },
                                      onChanged: (value) {
                                        dod = value;
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Definition of Done",
                                          hasFloatingPlaceholder: true
                                      ),
                                      initialValue: dod,
                                    ),
                                  ),
                                ],
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('Save'),
                                  onPressed: () {
                                    pr.show().whenComplete(() {
                                      updateDod();
                                    });
                                  },
                                )
                              ],
                            );
                          });
                    },
                  ),
                ],
              ),
            );
          });
    }

    List<Widget> makeAppBar(String role) {
      if (role == 'Product Owner') {
        return <Widget>[new Padding(
          padding: EdgeInsets.only(right: 12.0),
          child: new InkWell(
            child: new Icon(
              Icons.refresh,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: refreshData,
          ),
        ), new Padding(
          padding: EdgeInsets.only(right: 15.0),
          child: new InkWell(
            child: new Icon(
              Icons.add,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () => _openAddPickerModal(context),
          ),
        ),
        ];
      } else if (role == "Scrum Master" || role == "Developer") {
        return <Widget>[new Padding(
          padding: EdgeInsets.only(right: 13.0),
          child: new InkWell(
            child: new Icon(
              Icons.refresh,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () {
              refreshData();
            },
          ),
        ),
          new Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: new InkWell(
              child: new Icon(
                Icons.people,
                color: util.hexToColor("#FFFFFF"),
              ),
              onTap: redirectTolistPeople,
            ),
          ),
        ];
      }
    }

    List<Widget> makeContent() {
      if (role == "Product Owner") {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.dashboard,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Dashboard",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.close,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
      } else {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.dashboard,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  'Dashboard',
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.help,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Guidance",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.close,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
      }
    }
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      appBar: AppBar(
        leading: new InkWell(
          child: new Icon(
            Icons.menu,
            color: util.hexToColor("#FFFFFF"),
          ),
          onTap: () {
            scaffoldKey.currentState.openDrawer();
          },
        ),
        centerTitle: true,
        title: Text(
          "Scrum Board".toUpperCase(),
          style: TextStyle(
            color: util.hexToColor("#FFFFFF"),
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: makeAppBar(role),
      ),
      body: isLoad? Center(
        child: new CircularProgressIndicator(),
      ) : new SingleChildScrollView(scrollDirection: Axis.vertical,child: makeBody),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: makeContent(),
          ),
        ),
      );
  }
}