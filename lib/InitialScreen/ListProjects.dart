import 'package:flutter/material.dart';
import 'package:skripsi_wins/InitialScreen/Dashboard.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skripsi_wins/InitialScreen/Profile.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/contents/DashboardAPI/Model.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';

class ListProjects extends StatefulWidget {

  @override
  _ListProjectsState createState() => _ListProjectsState();
}

class _ListProjectsState extends State<ListProjects> {

  var formKey, addtaskformkey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final util = new Util();
  List<Project> arrProject = new List<Project>();
  bool isLoad;
  dynamic response;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  String username, role = "";
  ProgressDialog pr;

  @override
  void initState() {
    getDataCache().whenComplete(() {
      getListProject().whenComplete(() {
        setState(() {
          isLoad = false;
        });
      });
    });
  }

  Future getListProject() async {
    response = await apiProvider.getListProjects(username);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else if (response['data'].length >= 1) {
      for(var i = 0; i < response['data'].length; i++) {
        var arrTemp = response['data'][i].toString().split(',');
        arrProject.add(new Project(arrTemp[1], int.parse(arrTemp[0])));
      }
    }
  }

  Future getDataCache() async {
    isLoad = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.getString("username");
    role = prefs.getString("role");
  }

  Future setProjID(int proj_id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("proj_id", proj_id);
  }

  void refreshData() {
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ListProjects()
      ),
    );
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    ListTile makeListTile(Project proj) =>
        ListTile(
          contentPadding:
          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          title: Text(
            proj.proj_name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

          trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
          onTap: () {
            setProjID(proj.proj_id).whenComplete(() {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Dashboard()));
            });
          },
        );

    Card makeCard(Project proj) {
      return new Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child:
        new Container(
          decoration: BoxDecoration(color: Colors.blue),
          child: makeListTile(proj),
        ),
      );
    }

    final makeBody = new Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Column(
        children: <Widget>[
          ListView.builder(
            physics: ScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: arrProject.length,
            itemBuilder: (BuildContext context, int index) {
              return makeCard(arrProject[index]);
            },
          ),
        ],
      ),
    );

    List<Widget> makeAppBar() {
        return <Widget>[new Padding(
          padding: EdgeInsets.only(right: 25.0),
          child: new InkWell(
            child: new Icon(
              Icons.refresh,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () {
              refreshData();
            },
          ),
        ),
        ];
    }

    List<Widget> makeContent() {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.close,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
    }
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      appBar: AppBar(
        leading: new InkWell(
          child: new Icon(
            Icons.menu,
            color: util.hexToColor("#FFFFFF"),
          ),
          onTap: () {
            scaffoldKey.currentState.openDrawer();
          },
        ),
        centerTitle: true,
        title: Text(
          "List Projects".toUpperCase(),
          style: TextStyle(
            color: util.hexToColor("#FFFFFF"),
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: makeAppBar(),
      ),
      body: DoubleBackToCloseApp(
      snackBar: const SnackBar(content: Text('Tap again to leave')),
      child: isLoad? Center(child: new CircularProgressIndicator()) : arrProject.length == 0? Center(child: new Text("Maaf anda belum terdaftar pada proyek")) :
      makeBody,
    ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: makeContent(),
        ),
      ),
    );
  }
}