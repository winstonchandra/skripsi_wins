import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/InitialScreen/ListProjects.dart';
import 'package:skripsi_wins/InitialScreen/Login.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skripsi_wins/InitialScreen/Dashboard.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:skripsi_wins/InitialScreen/CreateProject.dart';

class Register extends StatefulWidget {

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final List<String> arrRole = <String>["Product Owner", "Scrum Master", "Developer"].toList();
  String roleChoosen;
  final formkey = GlobalKey<FormState>();
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  bool autoValidate = false;
  bool completeRegis = false;
  String name, username, email, password;
  dynamic response;
  ProgressDialog pr;

  void _validateInputs() {
    if (formkey.currentState.validate() & (this.roleChoosen != '')) {
        formkey.currentState.save();
        regis().whenComplete(() {
          if (completeRegis) {
            saveDataCache().whenComplete(() {
              pr.hide().whenComplete(() {
                Navigator.of(context).pop();
                if (roleChoosen == "Product Owner") {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CreateProject()));
                } else if (roleChoosen == "Scrum Master") {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ListProjects()));
                } else {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Dashboard()));
                }
              });
            });
          }
        });
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            autoValidate = true;
          });
        });
      }
    }
  }

  Future saveDataCache() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("username", username);
    prefs.setString("role", roleChoosen);
  }

  Future regis() async {
    var bytes = utf8.encode(password);
    String digest = sha512.convert(bytes).toString();
    response = await apiProvider.saveUserDatatoBackend(name, username, digest, roleChoosen, email);
    if (response.toString() == "500") {
      pr.hide();
      completeRegis = false;
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
      });
    } else if (response['result'] == 'not_valid') {
      pr.hide();
      completeRegis = false;
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Username sudah terdaftar'),
              content: Text('Mohon mengganti username yang anda masukkan'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      completeRegis = true;
    }
  }


  Widget singUpCard(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    final roleDropdownItems = arrRole.map((String value) { return DropdownMenuItem(
        child: Text(value),
        value: value,
      );
    }).toList();

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: _height / 45),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: new Form (
            key: formkey,
            autovalidate: autoValidate,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 8,
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Create Account",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 28,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      autofocus: false,
                      validator: (String value) {
                        return value.isEmpty? 'Please fill the data' : null;
                      },
                      onSaved: (String value) {name = value;},
                      decoration: InputDecoration(
                          labelText: "Name", hasFloatingPlaceholder: true),
                    ),
                    TextFormField(
                      autofocus: false,
                      validator: (String value) {
                        return value.isEmpty? 'Please fill the data' : null;
                      },
                      onSaved: (String value) {username = value;},
                      decoration: InputDecoration(
                          labelText: "Username", hasFloatingPlaceholder: true),
                    ),
                    TextFormField(
                        autofocus: false,
                        keyboardType: TextInputType.emailAddress,
                        validator: (String value) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = new RegExp(pattern);
                          return !regex.hasMatch(value)? 'Enter valid email' : null;
                        },
                        onSaved: (String value) {email = value;},
                        decoration: InputDecoration(
                            labelText: "Email", hintText: "ab@gmail.com"
                        )
                    ),
                    TextFormField(
                      autofocus: false,
                      obscureText: true,
                      validator: (String value) {
                        return value.isEmpty? 'Please fill the data' : null;
                      },
                      onSaved: (String value) {password = value;},
                      decoration: InputDecoration(
                          labelText: "Password", hasFloatingPlaceholder: true),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right:102.0),
                      child: Listener(
                        onPointerDown: (_) => FocusScope.of(context).unfocus(),
                        child: DropdownButton<String>(
                          icon: Icon(Icons.arrow_drop_down),
                          hint: Text("Select role"),
                          value: roleChoosen,
                          items: roleDropdownItems,
                          onChanged: (String value) {
                            setState(() {
                              roleChoosen = value;
                            });
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 180.0, bottom: 11.0),
                      child: Text(
                        roleChoosen == null? 'Choose Role!': '',
                        style: TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(),
                        ),
                        FlatButton(
                          child: Text("Sign Up"),
                          color: Color(0xFF4B9DFE),
                          textColor: Colors.white,
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 15, bottom: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () {
                            pr.show().whenComplete(() {
                              _validateInputs();
                            });
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Text(
              "Already have an account?",
              style: TextStyle(color: Colors.black),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Login()));
              },
              textColor: Colors.orange,
              child: Text("Login"),
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top:40.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 100.0,
                  child: Image.asset(
                    "assets/logos/logo-color.png",
                    fit: BoxFit.contain,
                    key: new Key("Scrum Booster Logo"),
                  ),
                ),
                singUpCard(context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}