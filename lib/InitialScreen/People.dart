import 'package:flutter/material.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:skripsi_wins/contents/DashboardAPI/Model.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class People extends StatefulWidget {

  int proj_id;
  String role;

  People(
      {this.proj_id, this.role});

  @override
  _PeopleState createState() => _PeopleState();
}

class _PeopleState extends State<People> {
  final formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  List peopleInProject = new List();
  String searchPerson, role;
  final util = new Util();
  bool isload;
  dynamic response;
  ProgressDialog pr;
  Icon iconButton;
  var dataPersons;

  @override
  void initState() {
    getListFriends().whenComplete(() {
      setState(() {
        isload = false;
      });
    });
  }

  Future getListFriends() async {
    isload = true;
    response = await apiProvider.getPeople(widget.proj_id);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else if (response['data'].length == 0) {
      peopleInProject = [];
      return ;
    }
    dataPersons = response['data'];
    for(var i = 0; i < response['data'].length; i++) {
      peopleInProject.add(new Person(username: response['data'][i]['username'], fullname: response['data'][i]['fullname'],
          role: response['data'][i]['role']));
    }
  }

  addFriend(String username, role) async {
    for (var i = 0; i < dataPersons.length; i++) {
      if (dataPersons[i]['role'] == role && role != "Developer") {
        pr.hide();
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Role sudah ada'),
                content: Text('Anda tidak bisa menambahkan role ini karena sudah ada pengguna dengan role tersebut di dalam proyek'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    }
    var dt = DateTime.now();
    var newFormat = DateFormat("yyyy-MM-dd");
    String date = newFormat.format(dt);
    response = await apiProvider.addPerson(widget.proj_id, username, role, date);
    if (response.toString() == "500") {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    pr.hide();
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => People(proj_id: widget.proj_id),
      ),
    );
  }

  Future search() async {
    if (formkey.currentState.validate()) {
      response = await apiProvider.findPerson(searchPerson);
      if (response.toString() == "500")  {
        pr.hide();
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Terjadi kesalahan pada server'),
                content: Text('Mohon ulangi permintaan kembali'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else if (response['result'] == "500")  {
        pr.hide();
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Username tidak ditemukan'),
                content: Text('Mohon masukkan username yang tepat'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else {
        pr.hide();
        showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Add Person'),
                content: tilePerson(),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
        if (response['data']['proj_id'] != -1) {
          showDialog(context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Username sudah terdaftar'),
                  content: Text('Anda tidak dapat menambahkan pengguna ini karena pengguna sudah terdaftar pada sebuah proyek'),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Close'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              });
        }
      }
    }
  }

  ListTile tilePerson() => ListTile(
    contentPadding:
    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
    title: Text(
      "Username: "+response['data']['username'],
      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
    ),
    // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

    subtitle: Row(
      children: <Widget>[
        Expanded(
            flex: 1,
            child: Container(
              // tag: 'hero',
              child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text("Name: "+response['data']['fullname']+" - Role: "+response['data']['role'],
                      style: TextStyle(color: Colors.black))
              ),
            )
        ),
      ],
    ),
    trailing: response['data']['proj_id'] == -1? iconButton = Icon(Icons.add, color: Colors.black, size: 30.0) : iconButton = null,
    onTap: () {
      if (iconButton != null) {
        pr.show().whenComplete(() {
          addFriend(response['data']['username'], response['data']['role']);
        });
      }
    },
  );

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    void back() {
      Navigator.of(context).pop();
    }

    ListTile makeListTile(Person person) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      title: Text(
        "Username: "+person.username,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

      subtitle: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Container(
                // tag: 'hero',
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text("Name: "+person.fullname+" - Role: "+person.role,
                        style: TextStyle(color: Colors.white))
                ),
              )),
        ],
      ),
    );

    Card makeCard(Person person) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.lightBlue),
        child: makeListTile(person),
      ),
    );

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: peopleInProject.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(peopleInProject[index]);
        },
      ),
    );

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: new InkWell(
          child: new Icon(
            Icons.arrow_back,
            color: util.hexToColor("#FFFFFF"),
          ),
          onTap: () {
            back();
          },
        ),
        centerTitle: true,
        title: Text(
          "List People".toUpperCase(),
          style: TextStyle(
            color: util.hexToColor("#FFFFFF"),
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          widget.role == "Product Owner"?
          new Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: new InkWell(
              child: new Icon(
                Icons.add,
                color: util.hexToColor("#FFFFFF"),
              ),
              onTap: () {
                return showDialog(context: context,
                    builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Search Person'),
                          content: Stack(
                            overflow: Overflow.visible,
                            children: <Widget>[
                              new Form(
                                key: formkey,
                                autovalidate: autoValidate,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: new TextFormField(
                                        onChanged: (value) {
                                          searchPerson = value;
                                        },
                                        decoration: InputDecoration(
                                            labelText: "Username",
                                            hasFloatingPlaceholder: true),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: RaisedButton(
                                        child: Text("Search"),
                                        onPressed: () {
                                          pr.show().whenComplete(() {
                                            search();
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                     }
                     );
              },
            ),
          ) : Container(),
        ],
      ),
      body: isload? Center(child:new CircularProgressIndicator()) : makeBody,
    );
  }
}