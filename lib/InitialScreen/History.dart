import 'package:flutter/material.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:skripsi_wins/contents/DashboardAPI/Model.dart';

class History extends StatefulWidget {

  int proj_id;

  History({
    this.proj_id
  });

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final formkey = GlobalKey<FormState>();
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  final util = new Util();
  dynamic response;
  bool isload;
  Map historyData = new Map();

  @override
  void initState() {
    isload = true;
    getData().whenComplete(() {
      setState(() {
        isload = false;
      });
    });
  }

  Future getData() async {
    response = await apiProvider.getHistory(widget.proj_id);
    if (response.toString() == "500") {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      historyData = response['data'];
    }
  }

  void back() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    List<charts.Series<dynamic, String>> _HistoryData() {
      List<HistoryData> data = [];
      List<HistoryData> data2 = [];
      historyData.forEach((k,v) => data.add(new HistoryData(k, v[0])));
      historyData.forEach((k,v) => data2.add(new HistoryData(k, v[1])));

      return [
        new charts.Series<HistoryData, String>(
          id: 'In Progress',
          domainFn: (HistoryData prog, _) => prog.sprint_name,
          measureFn: (HistoryData prog , _) => prog.velocities,
          data: data,
          labelAccessorFn: (HistoryData prog, _) => prog.velocities.toString()
        ),
        new charts.Series<HistoryData, String>(
          id: 'Done',
          domainFn: (HistoryData done, _) => done.sprint_name,
          measureFn: (HistoryData done , _) => done.velocities,
          data: data2,
          labelAccessorFn: (HistoryData done, _) => done.velocities.toString()
        ),
      ];
    }

    Widget makeBody() {
      return new Container(
          child: new ListView(
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),
            children: <Widget>[
              new Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                      child: Center(
                        child: new Text(
                          "Team per Sprint Velocities Growth",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: _height/1.8,
                      width: _width/0.95,
                      child: new charts.BarChart(_HistoryData(),
                          animate: false,
                          barRendererDecorator: new charts.BarLabelDecorator<String>(),
                          barGroupingType: charts.BarGroupingType.grouped,
                          behaviors: [new charts.ChartTitle('Velocities',
                              behaviorPosition: charts.BehaviorPosition.start,
                              titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                            new charts.ChartTitle('Sprint Period',
                                behaviorPosition: charts.BehaviorPosition.bottom,
                                titleOutsideJustification:
                                charts.OutsideJustification.middleDrawArea),
                            new charts.SeriesLegend(), new charts.SlidingViewport(), new charts.PanAndZoomBehavior()],
                          domainAxis: new charts.OrdinalAxisSpec(viewport: new charts.OrdinalViewport("Sprint 1", 4)),

                      ),
                    )
                  ]
              ),
            ],
          )
      );
    }

    return new Scaffold(
        appBar: AppBar(
          leading: new InkWell(
            child: new Icon(
              Icons.arrow_back,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () {
              back();
            },
          ),
          centerTitle: true,
          title: Text(
            "History".toUpperCase(),
            style: TextStyle(
              color: util.hexToColor("#FFFFFF"),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: isload? Center(child: new CircularProgressIndicator()) : makeBody()
    );
  }
}