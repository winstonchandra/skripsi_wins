import 'package:flutter/material.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';

class Profile extends StatefulWidget {

  String username;

  Profile({
    this.username
  });

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  final util = new Util();
  dynamic response;
  bool isload, checkErrorEmail;
  bool isVisible = false;
  Map<String, String> mapUpdate = new Map();
  String username, role, fullname, fullnameAwal, emailAwal, email = "";
  ProgressDialog pr;

  @override
  void initState() {
    detailProfile().whenComplete(() {
      setState(() {
        isload = false;
      });
    });
  }

  updateProfile() async {
    if (checkErrorEmail != true) {
      response = await apiProvider.updateProfile(mapUpdate);
      if (response.toString() == "500") {
        pr.hide();
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Terjadi kesalahan pada server'),
                content: Text('Mohon ulangi permintaan kembali'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else {
        if (pr.isShowing()) {
          pr.hide().whenComplete(() {
            setState(() {
              isVisible = false;
            });
          });
        }
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Proses berhasil'),
                content: Text('Profil anda telah diupdate'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            isVisible = true;
          });
        });
      }
    }
  }

  Future detailProfile() async {
    isload = true;
    response = await apiProvider.detailProfile(widget.username);
    if (response.toString() == '500') {
      return showDialog(context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Terjadi kesalahan pada server'),
            content: Text('Mohon ulangi permintaan kembali'),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
    }
    username = response['data']['username'];
    mapUpdate['username'] = username;
    fullnameAwal = response['data']['fullname'];
    role = response['data']['role'];
    emailAwal = response['data']['email'];
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
    );

    void back() {
      Navigator.pop(context);
    }

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: new Form(
        key: formkey,
        autovalidate: autoValidate,
        child: new Container(
          color: Color(0xffFFFFFF),
          child: Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Username",
                                hasFloatingPlaceholder: true),
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            enabled: false,
                            initialValue: username,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Fullname",
                                hasFloatingPlaceholder: true),
                            onChanged: (text) {
                              if (text != fullname && text != "" && email != "") {
                                fullname = text;
                                mapUpdate['fullname'] = fullname;
                                setState(() {
                                  isVisible = true;
                                });
                              } else {
                                fullname = text;
                                setState(() {
                                  isVisible = false;
                                });
                              }
                            },
                            initialValue: fullnameAwal,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            decoration: InputDecoration(
                                labelText: "Role",
                                hasFloatingPlaceholder: true),
                            validator: (String value) {
                              return value.isEmpty? 'Please fill the data' : null;
                            },
                            enabled: false,
                            initialValue: role,
                          ),
                        ),
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Flexible(
                          child: new TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                labelText: "Email",
                                hasFloatingPlaceholder: true),
                            initialValue: emailAwal,
                            onChanged: (text) {
                              if (text != emailAwal && text != "" || (fullname != "" && fullname != fullnameAwal)) {
                                Pattern pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                RegExp regex = new RegExp(pattern);
                                if (regex.hasMatch(text)) {
                                  email = text;
                                  mapUpdate['email'] = email;
                                  setState(() {
                                    checkErrorEmail = false;
                                    isVisible = true;
                                  });
                                } else {
                                  setState(() {
                                    checkErrorEmail = true;
                                    isVisible = false;
                                  });
                                }
                              } else {
                                email = text;
                                setState(() {
                                  checkErrorEmail = false;
                                  isVisible = false;
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 10.0,left: 25.0, right: 100.0, bottom: 8.0),
                  child: Text(
                    checkErrorEmail == true? 'Please enter valid email': '',
                    style: TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                  ),
                ),
                Visibility(
                  visible: isVisible,
                  child: Padding(
                    padding: EdgeInsets.only(left: 245.0, top: 15.0),
                    child: Container (color: Colors.redAccent, child:FlatButton(
                      textColor: Colors.white,
                      child: Text('Save'),
                      onPressed: () {
                        pr.show();
                        updateProfile();
                      },
                      shape: CircleBorder(),
                    ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: new InkWell(
            child: new Icon(
              Icons.arrow_back,
              color: util.hexToColor("#FFFFFF"),
            ),
            onTap: () {
              back();
            },
          ),
          centerTitle: true,
          title: Text(
            "Profile".toUpperCase(),
            style: TextStyle(
              color: util.hexToColor("#FFFFFF"),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: isload? Center(child: new CircularProgressIndicator()) : makeBody
    );
  }
}