import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:skripsi_wins/InitialScreen/BurndownChartData.dart';
import 'package:skripsi_wins/InitialScreen/History.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:skripsi_wins/InitialScreen/HomeScreen.dart';
import 'package:skripsi_wins/InitialScreen/Profile.dart';
import 'package:skripsi_wins/InitialScreen/ScrumBoard.dart';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:skripsi_wins/contents/DashboardAPI/Model.dart';
import 'package:intl/intl.dart';
import 'package:flutter_speedometer/flutter_speedometer.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';

class Dashboard extends StatefulWidget {

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  final Util util = new Util();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  bool isload = false;
  String role;
  String username;
  int ProjID = -1;
  List sprintBurndown = new List();
  dynamic response;
  Map smData = new Map();
  DateTime backbuttonpressedTime;
  List arrDateSprintBurndown = new List();
  List arrRemainSprintBurndown = new List();
  List arrExpectSprintBurndown = new List();
  List arrDateDev = new List();
  List arrRemainDev = new List();
  List arrDoneDev = new List();
  int cummVelDev =  0;
  int total_vel_proj = 0;
  String pivotSM = "";

  @override
  void initState() {
    checkDataCache().whenComplete(() {
      if (role == "Product Owner") {
        checkPOSBC().whenComplete(() {
          getPOData().whenComplete(() {
            setState(() {
              isload = false;
            });
          });
        });
      } else if ((role == "Scrum Master") && ((ProjID != -1) && (ProjID != null))) {
        getPOData().whenComplete(() {
          getSMData().whenComplete(() {
            setState(() {
              isload = false;
            });
          });
        });
      } else if ((role == "Developer") && ((ProjID != -1) && (ProjID != null))) {
        getPOData().whenComplete(() {
          getDevData().whenComplete(() {
            setState(() {
              isload = false;
            });
          });
        });
      }
      setState(() {
        isload = false;
      });
    });
  }

  Future getSMData() async {
    response = await apiProvider.getSMData(ProjID);
    if (response.toString() == '500') {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    smData = response['data'];
    total_vel_proj = response['tot_vel'];
  }

  Future checkPOSBC() async {
    var dt = DateTime.now();
    var newFormat = DateFormat("yyyy-MM-dd");
    String checkerDay = DateFormat('EEEE').format(dt);
    String date = newFormat.format(dt);
    response = await apiProvider.checkposbc(username, date, checkerDay, ProjID);
    if (response.toString() == '500') {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else if (response['data'] == "fill") {
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Burndown(proj_id: ProjID, username: username, flagFill: "seterusnya")
        ),
      );
    }
  }

  Future getDevData() async {
    var date = DateTime.now();
    var format = DateFormat("yyyy-MM-dd");
    String day = DateFormat('EEEE').format(date);
    String dateNow = format.format(date);
    response = await apiProvider.getDevData(username, dateNow, day, ProjID);
    if (response.toString() == '500') {
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
    arrDateDev = response['data']['date'];
    arrRemainDev = response['data']['remain'];
    arrDoneDev = response['data']['done'];
    cummVelDev = response['data']['cumm'];
    total_vel_proj = response['tot_vel'];
  }

  Future getPOData() async {
    response = await apiProvider.getPOData(ProjID);
    if (response.toString() == '500') {
      return showDialog(context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Terjadi kesalahan pada server'),
            content: Text('Mohon ulangi permintaan kembali'),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
    }
    arrDateSprintBurndown = response['data']['date'];
    arrExpectSprintBurndown = response['data']['expect'];
    arrRemainSprintBurndown = response['data']['remain'];
  }

  Future checkDataCache() async {
    isload = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString("username");
    role = await prefs.getString("role");
    var projIDCache = await prefs.getInt("proj_id");
    if (projIDCache != null) {
      ProjID = projIDCache;
    } else {
      response = await apiProvider.checkUser(username);
      if (response.toString() == '500') {
        return showDialog(context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Terjadi kesalahan pada server'),
                content: Text('Mohon ulangi permintaan kembali'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else if (response['proj_id'] == null) {
        ProjID = -1;
        return ;
      } else {
        ProjID = response['proj_id'];
      }
    }
  }

  void refreshData() {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Dashboard()
      ),
    );
  }

  void history() {
    if (ProjID == -1) {
      showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Belum Terdaftar'),
              content: Text('Mohon maaf anda belum terdaftar pada proyek'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => History(proj_id: ProjID)
        ),
      );
    }
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  @override
  Widget build(BuildContext context) {

    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    List<charts.Series<dynamic, DateTime>> _sprintBurndownData() {
      List<SprintBurndownExpect> data = [];
      List<SprintBurndownRem> data2 = [];
      for (var i = 0; i < arrDateSprintBurndown.length; i++) {
        var strDate = arrDateSprintBurndown[i];
        var arrTemp = strDate.split('-');
        var year = int.parse(arrTemp[0]); var month = int.parse(arrTemp[1]); var date = int.parse(arrTemp[2]);
        data.add(new SprintBurndownExpect(new DateTime(year, month, date), arrExpectSprintBurndown[i]));
        if (i < arrRemainSprintBurndown.length) {
          data2.add(new SprintBurndownRem(
              new DateTime(year, month, date), arrRemainSprintBurndown[i]));
        }
      }

      return [
        new charts.Series<SprintBurndownExpect, DateTime>(
          id: 'Sprint Burndown Expectation',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (SprintBurndownExpect burn, _) => burn.date,
          measureFn: (SprintBurndownExpect burn , _) => burn.velocities,
          data: data,
        ),
        new charts.Series<SprintBurndownRem, DateTime>(
          id: 'Sprint Burndown Remaining',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (SprintBurndownRem burn, _) => burn.date,
          measureFn: (SprintBurndownRem burn , _) => burn.velocities,
          data: data2,
        ),
      ];
    }

    List<charts.Series<dynamic, DateTime>> _devProgressData() {
      List<DevDonePerDayVel> data = [];
      List<DevRemPerDayVel> data2 = [];
      for (var i = 0; i < arrDateDev.length; i++) {
        var strDate = arrDateDev[i];
        var arrTemp = strDate.split('-');
        var year = int.parse(arrTemp[0]); var month = int.parse(arrTemp[1]); var date = int.parse(arrTemp[2]);
        data.add(new DevDonePerDayVel(new DateTime(year, month, date), arrDoneDev[i]));
        data2.add(new DevRemPerDayVel(
              new DateTime(year, month, date), arrRemainDev[i]));
      }

      return [
        new charts.Series<DevDonePerDayVel, DateTime>(
          id: 'Developer Done',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          domainFn: (DevDonePerDayVel done, _) => done.date,
          measureFn: (DevDonePerDayVel done , _) => done.velocities,
          data: data,
        ),
        new charts.Series<DevRemPerDayVel, DateTime>(
          id: 'Developer Remaining',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (DevRemPerDayVel rem, _) => rem.date,
          measureFn: (DevRemPerDayVel rem , _) => rem.velocities,
          data: data2,
        ),
      ];
    }

    List<charts.Series<dynamic, String>> _SMData() {
      List<SMData> data = [];
      List<SMData> data2 = [];
      smData.forEach((k,v) => data.add(new SMData(k, v[0])));
      smData.forEach((k,v) => data2.add(new SMData(k, v[1])));
      if (smData.length >= 1) {
        var _list = smData.keys.toList();
        pivotSM = _list[0];
      }

      return [
        new charts.Series<SMData, String>(
          id: 'Cumulative Done ',
          domainFn: (SMData done, _) => done.name,
          measureFn: (SMData done , _) => done.velocities,
          data: data,
          labelAccessorFn: (SMData done, _) => done.velocities.toString()
        ),
        new charts.Series<SMData, String>(
          id: 'Remain',
          domainFn: (SMData rem, _) => rem.name,
          measureFn: (SMData rem , _) => rem.velocities,
          data: data2,
          labelAccessorFn: (SMData rem, _) => rem.velocities.toString()
        ),
      ];
    }

    Widget makeBodyPO() {

      return new Container(
        child: new ListView(
          scrollDirection: Axis.vertical,
          physics: ScrollPhysics(),
          children: <Widget>[
            new Column(
              children: <Widget>[
                  Container(
                    height: _height/2,
                    width: _width/0.95,
                    child: new charts.TimeSeriesChart(_sprintBurndownData(),
                      animate: false,
                      defaultRenderer: new charts.LineRendererConfig(includePoints: true),
                      behaviors: [
                        new charts.ChartTitle('Sprint Burndown Chart',
                          subTitle: 'Red: Team Expectation, Blue: Team Realization',
                        ),
                        new charts.ChartTitle('Velocities',
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea),
                      ],
                    ),
                  ),
              ]
            ),
          ],
        )
      );
    }

    Widget makeBodySM() {
      return new Container(
          child: new ListView(
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),
            children: <Widget>[
              new Column(
                  children: <Widget>[
                    Container(
                      height: _height/2,
                      width: _width/0.95,
                      child: new charts.TimeSeriesChart(_sprintBurndownData(),
                        animate: false,
                        defaultRenderer: new charts.LineRendererConfig(includePoints: true),
                        behaviors: [
                          new charts.ChartTitle('Sprint Burndown Chart',
                            subTitle: 'Red: Team Expectation, Blue: Team Realization',
                          ),
                          new charts.ChartTitle('Velocities',
                              behaviorPosition: charts.BehaviorPosition.start,
                              titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:15.0),
                      child: Center(
                        child: new Text("Team Developer Velocities Progress",
                          style: TextStyle(fontWeight: FontWeight.bold
                              , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5.0),
                      child: Center(
                        child: new Text("of Total Project Velocities: "+total_vel_proj.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold
                              , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Container(
                      height: _height/2,
                      width: _width/0.9,
                      child: new charts.BarChart(_SMData(),
                        animate: false,
                        barRendererDecorator: new charts.BarLabelDecorator<String>(),
                        barGroupingType: charts.BarGroupingType.grouped,
                        behaviors: [new charts.ChartTitle('Velocities',
                            behaviorPosition: charts.BehaviorPosition.start,
                            titleOutsideJustification:
                            charts.OutsideJustification.middleDrawArea),
                          new charts.ChartTitle('Developer Team',
                              behaviorPosition: charts.BehaviorPosition.bottom,
                              titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                          new charts.SeriesLegend(), new charts.SlidingViewport(), new charts.PanAndZoomBehavior()],
                        domainAxis: new charts.OrdinalAxisSpec(
                          viewport: new charts.OrdinalViewport(pivotSM, 4)),
                      ),
                    )
                  ]
              ),
            ],
          )
      );
    }

    Widget makeBodyDev() {
      return new Container(
          child: new ListView(
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),
            children: <Widget>[
              new Column(
                  children: <Widget>[
                    Container(
                      height: _height/2,
                      width: _width/0.95,
                      child: new charts.TimeSeriesChart(_sprintBurndownData(),
                        animate: false,
                        defaultRenderer: new charts.LineRendererConfig(includePoints: true),
                        behaviors: [
                          new charts.ChartTitle('Sprint Burndown Chart',
                            subTitle: 'Red: Team Expectation, Blue: Team Realization',
                          ),
                          new charts.ChartTitle('Velocities',
                              behaviorPosition: charts.BehaviorPosition.start,
                              titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                        ],
                      ),
                    ),
                    Container(
                      height: _height/2,
                      width: _width/0.95,
                      child: new charts.TimeSeriesChart(_devProgressData(),
                        animate: false,
                        defaultRenderer: new charts.LineRendererConfig(includePoints: true),
                        behaviors: [
                          new charts.ChartTitle('Your Daily Velocities Progress',
                            subTitle: 'Red: Remaining, Green: Done',
                          ),
                          new charts.ChartTitle('Velocities',
                              behaviorPosition: charts.BehaviorPosition.start,
                              titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:15.0),
                      child: Center(
                        child: new Text("Your Cumulative Done Velocities",
                            style: TextStyle(fontWeight: FontWeight.bold
                            , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:1.0),
                      child: Center(
                        child: new Text("of Total Project Velocities: "+total_vel_proj.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold
                              , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:8.0),
                      child: Center(
                        child: new Text("Blue: Cumulative Velocities Done",
                          style: TextStyle(fontWeight: FontWeight.bold
                              , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5.0),
                      child: Center(
                        child: new Text("Red: Total Velocities Project",
                          style: TextStyle(fontWeight: FontWeight.bold
                              , fontSize: 16.0),
                        ),
                      ),
                    ),
                    Container(
                      height: _height/3.1,
                      width: _width/1.05,
                      child: new Speedometer(
                          minValue: 0,
                          maxValue: total_vel_proj,
                          backgroundColor: Colors.white,
                          currentValue: cummVelDev,
                          warningValue: cummVelDev,
                          kimColor: Colors.black,
                          meterColor: Colors.lightBlueAccent,
                          displayNumericStyle: TextStyle(
                            fontFamily: 'Digital-Display',
                            color: Colors.black,
                            fontSize: 15),
                          displayTextStyle: TextStyle(color: Colors.black, fontSize: 15),
                          displayText: "of "+total_vel_proj.toString(),
                      )
                    ),
                  ]
              ),
            ],
          )
      );
    }

    List<Widget> makeContent() {
      if (role == "Product Owner") {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.work,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Scrum Board",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScrumBoard(),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.close,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
      } else if (role == 'Scrum Master') {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.work,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  'Scrum Board',
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              if (ProjID == -1) {
                showDialog(context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Belum terdaftar'),
                        content: Text('Maaf, anda belum terdaftar pada proyek, mohon menghubungi product owner terkait untuk menambahkan anda ke dalam proyek'),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('Close'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      );
                    });
              } else {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ScrumBoard(),
                  ),
                );
              }
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.help,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Guidance",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(username: username,),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.apps,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "List Projects",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.book,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
      } else {
        return <Widget>[
          new DrawerHeader(
            child: Center(
              child: new Image.asset(
                "assets/logos/logo-color.png",
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.tag_faces,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Profile",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(username: username),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.work,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  'Scrum Board',
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              if (ProjID == -1) {
                showDialog(context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Belum terdaftar'),
                        content: Text('Maaf, anda belum terdaftar pada proyek, mohon menghubungi product owner terkait untuk menambahkan anda ke dalam proyek'),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('Close'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      );
                    });
              } else {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ScrumBoard(),
                  ),
                );
              }
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.help,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Guidance",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(username: username,),
                ),
              );
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Icon(
                  Icons.book,
                  color: util.hexToColor("#979797"),
                ),
                new Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text(
                  "Logout",
                  style: TextStyle(
                    color: util.hexToColor("#979797"),
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            onTap: () {
              logout();
              Navigator.popUntil(context, ModalRoute.withName('/Boarding'));
            },
          ),
        ];
      }
    }

    List<Widget> getAppBar(String role) {
      if (role == "Product Owner") {
        return <Widget>[
          new Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: new InkWell(
              child: new Icon(
                Icons.refresh,
                color: util.hexToColor("#FFFFFF"),
              ),
              onTap: refreshData,
            ),
          )
        ];
      } else {
        return <Widget>[
          new Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: new InkWell(
              child: new Icon(
                Icons.history,
                color: util.hexToColor("#FFFFFF"),
              ),
              onTap: history,
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: new InkWell(
              child: new Icon(
                Icons.refresh,
                color: util.hexToColor("#FFFFFF"),
              ),
              onTap: refreshData,
            ),
          )
        ];
      }
    }

    Widget makeBody() {
      if (role == "Product Owner") {
        return makeBodyPO();
      } else if (role == "Scrum Master") {
        return makeBodySM();
      } else {
        return makeBodyDev();
      }
    }

    Future<bool> onWillPop(BuildContext context) async {
      DateTime currentTime = DateTime.now();
      //Statement 1 Or statement2
      bool backButton = backbuttonpressedTime == null ||
          currentTime.difference(backbuttonpressedTime) > Duration(seconds: 3);
      if (backButton) {
        backbuttonpressedTime = currentTime;
        final snackbar = SnackBar(content: Text('Tap again to leave dashboard'));
        Scaffold.of(context).showSnackBar(snackbar);
        return false;
      }
      return true;
    }

    return Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: new InkWell(
            child: new Icon(
            Icons.menu,
            color: util.hexToColor("#FFFFFF"),
          ),
          onTap: () {
            scaffoldKey.currentState.openDrawer();
          },
        ),
        centerTitle: true,
        title: Text(
          "Dashboard".toUpperCase(),
          style: TextStyle(
            color: util.hexToColor("#FFFFFF"),
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: getAppBar(role),
      ),
      body: role == "Scrum Master"? (isload? Center(child: new CircularProgressIndicator()) : makeBody()) : DoubleBackToCloseApp(
        snackBar: const SnackBar(content: Text('Tap again to leave dashboard')),
        child: isload? Center(child: new CircularProgressIndicator()) : ProjID == -1? Center(child: new Text("Maaf anda belum terdaftar pada proyek")) :
        makeBody(),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: makeContent(),
        ),
      ),
    );
  }
}