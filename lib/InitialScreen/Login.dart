import 'package:flutter/material.dart';
import 'package:skripsi_wins/InitialScreen/ListProjects.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:skripsi_wins/InitialScreen/Dashboard.dart';
import 'package:skripsi_wins/InitialScreen/Registration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:skripsi_wins/contents/DashboardAPI/APIProvider.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final formkey = GlobalKey<FormState>();
  final DashboardAPIProvider apiProvider = new DashboardAPIProvider();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  String username, password;
  int proj_id = -1;
  Util util = new Util();
  dynamic response;
  bool isLoad, autoValidate, passwordVisible;
  ProgressDialog pr;

  @override
  void initState() {
    autoValidate = false;
    passwordVisible = true;
  }

  Future saveDatatoCache(String username, role, int proj_id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("username", username);
    prefs.setString("role", role);
    prefs.setInt("proj_id", proj_id);
  }

  getUsernameCache() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool username = await (prefs.get("username") != null) && prefs.getBool("username");
    return username? true : false;
  }

  checkDatatoDB(String username, String password) async {
    var bytes = utf8.encode(password);
    String digest = sha512.convert(bytes).toString();
    response = await apiProvider.checkLoginData(username, digest);
    if (response.toString() == "500") {
      pr.hide();
      return showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Terjadi kesalahan pada server'),
              content: Text('Mohon ulangi permintaan'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } else if (response['result'] == false) {
      pr.hide();
      return showDialog(context: context, 
          builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Input data salah'),
          content: Text('Mohon cek kembali input yang anda berikan'),
          actions: <Widget>[
            FlatButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
    } else {
      if (response['data']['role'] == "Scrum Master") {
        saveDatatoCache(username, response['data']['role'], 0).whenComplete(() {
          pr.hide();
          Navigator.of(context).pop();
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ListProjects()));
        });
      } else {
        if (response['data'].containsKey('proj_id')) {
          proj_id = response['data']['proj_id'];
        }
        saveDatatoCache(username, response['data']['role'], proj_id).whenComplete(() {
          pr.hide();
          Navigator.of(context).pop();
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Dashboard()));
        });
      }
    }
  }

  void _validateInputs() async {
    if (formkey.currentState.validate()) {
      formkey.currentState.save();
      checkDatatoDB(username, password);
    } else {
      if (pr.isShowing()) {
        pr.hide().whenComplete(() {
          setState(() {
            autoValidate = true;
          });
        });
      }
    }
  }

  void login(String username, String password) async {
    checkDatatoDB(username, password);
  }

  Widget _createAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Don\'t have an account ?',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Register()));
            },
            child: Text(
              'Register',
              style: TextStyle(
                  color: Color(0xfff79c4f),
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true
    );

    final usernameField = TextFormField(
      style: style,
      validator: (String value) {
        return value.isEmpty? 'Please fill the data' : null;
      },
      onSaved: (String value) {username = value;},
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Username",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextFormField(
      obscureText: passwordVisible,
      style: style,
      validator: (String value) {
        return value.isEmpty? 'Please fill the data' : null;
      },
      onSaved: (String value) {password = value;},
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                passwordVisible = !passwordVisible;
              });
            },
            icon: Icon(passwordVisible? Icons.visibility_off : Icons.visibility),
            color: Theme.of(context).primaryColorDark,
          ),
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = new InkWell(
      onTap: () {
        pr.show().whenComplete(() {
          _validateInputs();
        });
      },
      child: new Container(
        width: util.fitScreenSize(_width, 0.45),
        height: util.fitScreenSize(_height, 0.08),
        decoration: new BoxDecoration(
          color: util.hexToColor("#FF0000"),
          border: new Border.all(
            color: util.hexToColor("#FFFFFF"),
            width: 2.0,
          ),
          borderRadius: new BorderRadius.circular(20.0),
        ),
        child: new Center(
          child: new Text(
            'Login',
            style: new TextStyle(
              fontSize: 18.0,
              color: util.hexToColor("#FFFFFF"),
            ),
          ),
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: new Form(
              key: formkey,
              autovalidate: autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 155.0,
                    child: Image.asset(
                      "assets/logos/logo-color.png",
                      fit: BoxFit.contain,
                      key: new Key("Scrum Booster Logo"),
                    ),
                  ),
                  SizedBox(height: 45.0),
                  usernameField,
                  SizedBox(height: 25.0),
                  passwordField,
                  SizedBox(
                    height: 15.0,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: _createAccountLabel(),
                  ),
                  loginButon,
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}