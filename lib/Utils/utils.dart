// Kode-kode pada file ini dibuat oleh salah satu Tim PPL 2019 yang mengembangankan aplikasi Scrum Booster sebelum skripsi saya
// Saya hanya mengubah konfigurasi base_url saja pada file ini

import 'package:flutter/material.dart';

class Util {
  String call;

  void callUtil() => call = "call";

  Map<String, String> getConfiguration() {
    return {
      'base_url': "https://skripsi-wins.herokuapp.com/",
    };
  }

  Color hexToColor(String colorCode) {
    return new Color(int.parse(colorCode.substring(1, 7), radix: 16) + 0xFF000000);
  }

  double fitScreenSize(double main, double unit) {
    return main * unit;
  }
  
}

