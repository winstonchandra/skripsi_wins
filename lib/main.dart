// Kode-kode pada file ini dibuat oleh salah satu Tim PPL 2019 yang mengembangankan aplikasi Scrum Booster sebelum skripsi saya
// Saya hanya menghapus beberapa routing untuk halaman yang tidak terpakai pada penelitian

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:skripsi_wins/InitialScreen/splashScreen.dart';
import 'package:skripsi_wins/InitialScreen/BoardingPage.dart';
import 'package:skripsi_wins/Utils/utils.dart';
import 'package:flutter/widgets.dart';
//routes
import 'package:skripsi_wins/InitialScreen/HomeScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(ScrumBoosterApp());
  });
}

class ScrumBoosterApp extends StatefulWidget {
  @override
  _ScrumBoosterAppState createState() => _ScrumBoosterAppState();
}

class _ScrumBoosterAppState extends State<ScrumBoosterApp> {
  final appTitle = "Scrum Booster";
  final util = new Util();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      theme: new ThemeData(
        primarySwatch: Colors.lightBlue,
        primaryColor: Colors.lightBlue,
        cursorColor: Colors.white,
        fontFamily: 'Montserrat',
      ),
      home: new SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/Home': (BuildContext context) => new HomePage(),
        '/Boarding':(BuildContext context) => new BoardingPage(),
      },
    );
  }
}