# SCRUM Booster

### Penjelasan Produk

------

SCRUM Booster adalah sebuah *tools* yang bertujuan untuk mempermudah para pengguna SCRUM (terutama SCRUM Master) dalam pelaksanaan praktik metodologi pengembangan perangkat lunak SCRUM.

SCRUM Booster hadir dalam bentuk aplikasi ponsel pintar (*smartphone*) dan ditulis dalam platform **Android**. SCRUM Booster diharapkan dapat menjadi alat bantu yang ringkas, lengkap, dan mudah dijangkau oleh para pelaku SCRUM.


### Scrum Booster Dashboard

------
Terdapat produk tambahan yaitu, *Dashboard* pada aplikasi SCRUM Booster. *Dashboard* yang dihasilkan pada penelitian skripsi saya, berisi
*scrum metrics* untuk memantau perkembangan individu dari kinerja tim *scrum*. Diharapkan *dashboard* yang dihasilkan dapat membuat
tim *scrum* mampu untuk bekerja secara komunikatif dan *hyperproductive* agar dapat menghasilkan produk sesuai dengan jadwal yang sudah ditetapkan pada periode *sprint*. 

*Repository* ini merupakan *server frontend* dari pengembangan aplikasi Scrum Booster Dashboard. *Server frontend* tidak di-*deploy* seperti *server backend* dan basis data pada Heroku. Untuk menghasilkan *apk* dari *server frontend* agar dapat digunakan pada *smartphone* menggunakan *command script*, yaitu *flutter build apk*. Implementasi dari *server frontend* menggunakan *framework* Flutter dengan Dart sebagai bahasa pemrograman.

### Teknologi yang Digunakan

------

Dalam pengembangannya, SCRUM Booster menggunakan teknologi sebagai berikut:

- [**Flutter**](https://flutter.io/): *Cross-platform framework* yang digunakan untuk mengembangkan aplikasi mobile dari SCRUM Booster. Berbasis bahasa pemrograman **Dart**.
- [**Django REST Framework**](https://www.django-rest-framework.org/): Sebuah *framework* yang menyediakan antarmuka REST API sebagai media pertukaran data dengan *mobile app*.

### Referensi

------

Konten-konten dalam aplikasi ini merujuk pada:

1. Scrum Guide 2017 : https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-US.pdf
2. Jurnal Scrum Metrics for Hyperproductive Scrum Team  https://www.agilealliance.org/wp-content/uploads/2016/01/ScrumMetricsAgile2012.pdf